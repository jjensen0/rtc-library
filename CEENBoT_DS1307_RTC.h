/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef RTC_H_
#define RTC_H_
#include "capi324v221.h"
#include <string.h>


#define RTC_ADDR 0x68 // DS1307 RTC Slave address (1101000)
#define isLeapYear(year) ((year % 4) == 0)
#define hasThirtyDays(month) (month == 4 || month == 6 || month == 9 || month == 11)
#define hasThirtyOneDays(month) (!hasThirtyDays(month) && month != 2 )
#define RTC_ADDR 0x68
#define BCDtoINT(var) (((var & 0xf0) >> 4) * 10) + (var & 0x0f)
#define INTtoBCD(var) ((var / 10) << 4) + (var % 10)

typedef struct RTC_time_type_ST {
		int sec, min, hour, year, date, month;
		char day[9];
} RTC_time_type;
volatile RTC_time_type CEENBoT_DS1307_TIME;

char *CEENBoT_DS1307_getTime();
void CEENBoT_DS1307_update();

#endif /* RTC_H_ */
