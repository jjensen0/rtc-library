/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "CEENBoT_DS1307_RTC.h"


/**
Function: CEENBoT_DS1307_getTime
Return: A character array containing the information from the RTC
Details: If the RTC is available (I2C has to be open), it will return a string in the format of mm/dd/20yy hh:mm:ss.
NOTE: If an error is found (I2C not working), it will hijack the program and report issues communicating with clock on LCD as well as speaker beeps.
Example Use:
#include "CEENBoT_DS1307_RTC.h"
...
	LCD_printf("%s", CEENBoT_DS1307_getTime());
...
 */
char *CEENBoT_DS1307_getTime() {
	char buf[60];
	unsigned char sec, min, hours, day, date, month, year; // Storing raw reads from RTC NOTE: BCD VALUES!
#ifdef DEBUG
			UART_printf(UART_UART0, "Entering get_date.\n");
#endif
	if (I2C_MSTR_start(RTC_ADDR, I2C_MODE_MT) == I2C_STAT_OK) {
		I2C_MSTR_send(0x00); // Tell RTC that we want to start at seconds byte.
	} else { // If RTC is not available, alert user to error and block further execution.
		if (SYS_get_state(SUBSYS_LCD) == SUBSYS_CLOSED)
			LCD_open();
		LCD_clear();
		LCD_printf("Issues communicating\n");
		LCD_printf("with clock.\n\n\n");
		UART_printf(UART_UART0, "GET_DATE: Issues with clock.\n");
		if (SYS_get_state(SUBSYS_SPKR) == SUBSYS_CLOSED) {
			SPKR_open(SPKR_BEEP_MODE);
		}
		SPKR_beep(abs(440));
		TMRSRVC_delay(100);
		SPKR_beep(0);
		TMRSRVC_delay(10);
		while (1)
			;
	}
	//If it reached this point, should be ready to get.
	if (I2C_MSTR_start(RTC_ADDR, I2C_MODE_MR) == I2C_STAT_OK) {
		I2C_MSTR_get(&sec, TRUE);
		I2C_MSTR_get(&min, TRUE);
		I2C_MSTR_get(&hours, TRUE);
		I2C_MSTR_get(&day, TRUE);
		I2C_MSTR_get(&date, TRUE);
		I2C_MSTR_get(&month, TRUE);
		I2C_MSTR_get(&year, FALSE);
		I2C_MSTR_stop();
		switch (day) {
		case 1:				//Sunday
			strcpy(buf, "Sun");
			break;
		case 2:				//Monday
			strcpy(buf, "Mon");
			break;
		case 3:				//Tuesday
			strcpy(buf, "Tue");
			break;
		case 4:				//Wed
			strcpy(buf, "Wed");
			break;
		case 5:				//Thursday
			strcpy(buf, "Thu");
			break;
		case 6:				//Friday
			strcpy(buf, "Fri");
			break;
		case 7:				//Saturday
			strcpy(buf, "Sat");
			break;
		default:			// End of the world.
			strcpy(buf, "OMG");
			break;
		}
		strcat(buf, " ");
		char tmp[21];
		sprintf(tmp, "%d/%d/20%d %02d:%02d:%02d", BCDtoINT(month),
		BCDtoINT(date), BCDtoINT(year), BCDtoINT(hours), BCDtoINT(min),
		BCDtoINT(sec));
		strcat(buf, tmp);
#ifdef DEBUG
		UART_printf(UART_UART0, "GET DATE BUF: %s\n", buf);
#endif
	} else {
		LCD_clear();
		LCD_printf("Issues reading\n");
		LCD_printf("the clock.\n\n\n");
		UART_printf(UART_UART0, "GET_DATE: Issues with reading the clock.\n");
		if (SYS_get_state(SUBSYS_SPKR) == SUBSYS_CLOSED) {
			SPKR_open(SPKR_BEEP_MODE);
		}
		SPKR_beep(abs(440));
		TMRSRVC_delay(100);
		SPKR_beep(0);
		TMRSRVC_delay(10);
		while (1)
			;
	}
	return buf;
}
/**
Private function that isn't likely used by the outside. This will fix the clock halt bit on the RTC. No checks, just sets the MSB to 0.
 */
void __DS1307_fix_ch() {
	if (I2C_MSTR_start(RTC_ADDR, I2C_MODE_MT) == I2C_STAT_OK) {
		I2C_MSTR_send(0x00);
		CEENBoT_DS1307_TIME.sec &= 0x7f; //01111111
		I2C_MSTR_send(CEENBoT_DS1307_TIME.sec);
		I2C_MSTR_stop();
	}
}

/**
Function: CEENBoT_DS13407_setTime
Arguments: 	day		- Capitalized day of the week (IE: "Sunday")
			month	- Numerical month (IE: 1-12)
			date	- Numerical date (IE: 1-31, NO VALIDATION)
			year	- 2 digit year (IE: 14 for 2014, or 1 for 2001)
			hour	- Hour of the day (IE: 4 for 4:00pm)
			min		- minute of the hour (IE: 32 for 4:32pm)
			sec		- second of the minute (IE: 20 for 4:32:20)
Details: This will transfer the date to the RTC via I2C. No sanity checks.
Example use:
#include "CEENBoT_DS1307_RTC.h"
...
	CEENBoT_DS1307_setTime("Monday", 1, 13, 14, 12, 00, 00); // Set time to 12:00:00 on 1/13/2014 (a Monday)
...

 */
void CEENBoT_DS1307_setTime(char *day, int month, int date, int year, int hour, int min,
		int sec) {
	int iday = 7;
	if (!strcmp(day, "Sunday"))
		iday = 0;
	else if (!strcmp(day, "Monday"))
		iday = 1;
	else if (!strcmp(day, "Tuesday"))
		iday = 2;
	else if (!strcmp(day, "Wednesday"))
		iday = 3;
	else if (!strcmp(day, "Thursday"))
		iday = 4;
	else if (!strcmp(day, "Friday"))
		iday = 5;
	else if (!strcmp(day, "Saturday"))
		iday = 6;

	if (I2C_MSTR_start(RTC_ADDR, I2C_MODE_MT) == I2C_STAT_OK) {
		I2C_MSTR_send(0x00);
		I2C_MSTR_send(INTtoBCD(sec));
		I2C_MSTR_send(INTtoBCD(min));
		I2C_MSTR_send(INTtoBCD(hour));
		I2C_MSTR_send(INTtoBCD(iday));
		I2C_MSTR_send(INTtoBCD(date));
		I2C_MSTR_send(INTtoBCD(month));
		I2C_MSTR_send(INTtoBCD(year));
		I2C_MSTR_stop();
	}
}

/**
Function: CEENBoT_DS1307_update
Detail: Updates the built in structure CEENBoT_DS1307_TIME with the current time and date.
	NOTE: If the RTC is not available, it will hijack the program.
Members of the CEENBoT_DS1307_TIME structure:
	day		- Character string of the day of the week
	sec		- integer with seconds
	min		- integer with minutes
	hour	- integer with hour of day
	year	- integer with last two digits of year (IE: 14 for 2014)
	date	- integer with day of month
	month	- integer with month of year (IE: 12 for December, etc)
Example Use:
#include "CEENBoT_DS1307_RTC.h"
...
	CEENBoT_DS12307_update();
	LCD_printf("The day is: %s\n", CEENBoT_DS1307_TIME.day);
	LCD_printf("The month is: %d\n", CEENBoT_DS1307_TIME.month);
...
 */
void CEENBoT_DS1307_update() {
	unsigned char sec, min, hour, date, month, year, day;
	if (I2C_MSTR_start(RTC_ADDR, I2C_MODE_MT) == I2C_STAT_OK) {
		I2C_MSTR_send(0x00); // Tell RTC that we want to start at seconds byte.
	} else {
		if(SYS_get_state(SUBSYS_LCD) != SUBSYS_OPEN)
			LCD_open();
		LCD_clear();
		LCD_printf("%02x is not responding.", RTC_ADDR);
		while(1);
	}
	if (I2C_MSTR_start(RTC_ADDR, I2C_MODE_MR) == I2C_STAT_OK) {
		I2C_MSTR_get(&sec, TRUE);
		I2C_MSTR_get(&min, TRUE);
		I2C_MSTR_get(&hour, TRUE);
		I2C_MSTR_get(&day, TRUE);
		I2C_MSTR_get(&date, TRUE);
		I2C_MSTR_get(&month, TRUE);
		I2C_MSTR_get(&year, FALSE);
		I2C_MSTR_stop();
		if ((sec & 0x80) != 0x00) //Clock halt bit is set, need to fix before continue.
				{
			__DS1307_fix_ch();
		}
		CEENBoT_DS1307_TIME.sec = BCDtoINT(sec);
		CEENBoT_DS1307_TIME.min = BCDtoINT(min);
		CEENBoT_DS1307_TIME.hour = BCDtoINT(hour);
		CEENBoT_DS1307_TIME.date = BCDtoINT(date);
		CEENBoT_DS1307_TIME.month = BCDtoINT(month);
		CEENBoT_DS1307_TIME.year = BCDtoINT(year);
		switch (day) {
		case 1:
			strcpy(CEENBoT_DS1307_TIME.day, "Sunday");
			break;
		case 2:
			strcpy(CEENBoT_DS1307_TIME.day, "Monday");
			break;
		case 3:
			strcpy(CEENBoT_DS1307_TIME.day, "Tuesday");
			break;
		case 4:
			strcpy(CEENBoT_DS1307_TIME.day, "Wednesday");
			break;
		case 5:
			strcpy(CEENBoT_DS1307_TIME.day, "Thursday");
			break;
		case 6:
			strcpy(CEENBoT_DS1307_TIME.day, "Friday");
			break;
		case 7:
			strcpy(CEENBoT_DS1307_TIME.day, "Saturday");
			break;
		default:
			strcpy(CEENBoT_DS1307_TIME.day, "ERROR");
			break;
		}
	}
}
